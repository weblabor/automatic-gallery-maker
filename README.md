# Automatic Gallery Maker

**Description:** Create a gallery of photos automatically based on the images uploaded in the post with a tag.

**Use:** Just add a post and add any of the next tags: `gallery`, `photos`, `galeria`, `galeriafotos` or `galeriadefotos`

or in a page or post add the next shortcode `[show_gallery]` and it will render the gallery.