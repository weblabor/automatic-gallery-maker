<?php
/*
    Plugin Name: Automatic Gallery Maker
    Description: Create a gallery of photos automatically based on the images uploaded in the post with a tag.
    Author: Carlos Escobar
    Author URI: http://www.weblabor.mx
    Text Domain: automatic-gallery-maker
    Version: 1.0

    Licenced under the GNU GPL:

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

add_action( 'wp_enqueue_scripts', 'add_files_gallery' );
function add_files_gallery() {
    if ( !ifHasGalleryTag() ) 
        return;
    addAGMLibraries();
}

function addAGMLibraries() {
    wp_enqueue_style( 'galleria', plugin_dir_url( __FILE__ ) . '/style.css',false,'1.1','all');
    wp_enqueue_script( 'galleria', 'https://cdnjs.cloudflare.com/ajax/libs/galleria/1.5.5/galleria.min.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'galleria-theme', 'https://cdnjs.cloudflare.com/ajax/libs/galleria/1.5.5/themes/classic/galleria.classic.min.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'galleria-init', plugin_dir_url( __FILE__ ) . '/script.js', array ( 'jquery' ), 1.1, true);
}

function ifHasGalleryTag() {
    if ( !is_singular('post') || !check_if_has_tags_for_galleries() ) 
        return false;
    return true;
}

function check_if_has_tags_for_galleries() {
    $tags = ['galeriadefotos', 'galeriafotos', 'galeria', 'gallery', 'photos'];
    foreach ($tags as $tag) {
        if(has_tag($tag))
            return true;
    }
    return false;
}

add_filter( 'the_content', 'add_gallery_photos' ); 
function add_gallery_photos( $content ) { 
    if ( !ifHasGalleryTag() ) 
        return $content;

    $new_content = make_gallery();
    $new_content .= $content;
    return $new_content;
}

add_filter( 'get_post_metadata', 'remove_featured_image_on_single', 10, 4 ); 
function remove_featured_image_on_single( $value, $object_id, $meta_key, $single ) { 
    if ( !is_singular('post') || $meta_key!='_thumbnail_id' || !check_if_has_tags_for_galleries() ) 
        return $value;
    return '';
}

function make_gallery() {
    global $post;
    $imgs = get_children(array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image','orderby' => 'menu_order', 'numberposts' => -1));
    if ($imgs == true) {
        print "<div id='galleria'>";
        $i = 0;
        foreach($imgs as $id => $attachment) {
            $i++;
            $img = wp_get_attachment_image_src($id, 'full');
            $img_url = $img[0];
            print '<img src="'. $img_url.'" alt="img" /></a>';
        }
        print '</div>';
    }
}

//[show_gallery]
function show_gallery_func( $atts ){
    addAGMLibraries();
    return make_gallery();
}
add_shortcode( 'show_gallery', 'show_gallery_func' );

?>